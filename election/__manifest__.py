

{
    "name": "Election Managment",
    "summary": """ Mini projet Mastere MP2L
    
        """,
    "version": "12.0",
    "license": "AGPL-3",
    "author": "Hnen Abdelmajid",
    "website": "",
    "depends": ["base","website"],

    "data": [
        #"wizards/kpi_dashboard_menu.xml",
        #"security/security.xml",
        "security/ir.model.access.csv",
        "views/res_partner_inherit.xml",
        "views/election_dashboard.xml",


    ],
    
}
