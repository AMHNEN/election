
from odoo import fields, models,api,_


class Election(models.Model):
    _name = "election"
    _description = "Election home"

    name = fields.Char("name")
    condidat_id = fields.Many2one("res.partner",string='Condidate',domain=[('is_condidate', '=', True)]) #, domain=[('is_condidate', '=', True)]
    date = fields.Date("Date")
    image = fields.Binary(related='condidat_id.image')
    state = fields.Selection([('new', 'New'), ('confirmed', 'Confirme'), ('approved', 'Approved'), (
        'denied', 'Denied')],  default="new",  track_visibility='onchange')

#     Config
    type_election = fields.Many2one("election.config.type",string='Type election')
    country_election = fields.Many2one("election.config.country",string='Pays')
    city_election = fields.Many2one("election.config.city",string='Ville')
    location_election = fields.Many2one("election.config.location",string='Bureau')
    room_election = fields.Many2one("election.config.room",string='salle')






class ElectionConfigType(models.Model):
    _name = "election.config.type"
    _description = "Election Config Type"

    name = fields.Char("Type Election")


class ElectionConfigRoom(models.Model):
    _name = "election.config.room"
    _description = "Election Config Room"

    name = fields.Char("Salle")
    room_number = fields.Char("N° Salle")
    location_id = fields.Many2one('election.config.location')


class ElectionConfigLocation(models.Model):
    _name = "election.config.location"
    _description = "Election Config Location"

    name = fields.Char("Location")
    city_id = fields.Many2one('election.config.city')
    room_ids = fields.One2many('election.config.room','location_id')


class ElectionConfigCity(models.Model):
    _name = "election.config.city"
    _description = "Election Config City"

    name = fields.Char("Ville")
    country_id = fields.Many2one('election.config.country')
    location_ids = fields.One2many('election.config.location','city_id')

class ElectionConfigCountry(models.Model):
    _name = "election.config.country"
    _description = "Election Config country"

    name = fields.Char("Pays")
    city_ids = fields.One2many('election.config.city','country_id')

